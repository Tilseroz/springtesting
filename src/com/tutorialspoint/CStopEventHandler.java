package com.tutorialspoint;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStoppedEvent;

/**
 *
 * @author David Tilšer
 */
public class CStopEventHandler implements ApplicationListener<ContextStoppedEvent>{

   @Override
   public void onApplicationEvent(ContextStoppedEvent event) {
      System.out.println("ContextStoppedEvent Received");
   }
    
}
