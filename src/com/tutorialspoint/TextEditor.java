package com.tutorialspoint;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author David Tilšer
 */
public class TextEditor {

    private final SpellChecker spellChecker;

    @Autowired
    public TextEditor(SpellChecker spellChecker) {
        System.out.println("Inside TextEditor constructor.");
        this.spellChecker = spellChecker;
    }

    public void spellCheck() {
        spellChecker.checkSpelling();
    }
}
