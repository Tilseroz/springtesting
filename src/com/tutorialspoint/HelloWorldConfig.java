package com.tutorialspoint;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author David Tilšer
 */
@Configuration
public class HelloWorldConfig {

    @Bean
    public HelloWord helloWorld() {
        return new HelloWord();
    }
}
